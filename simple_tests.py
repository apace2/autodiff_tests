import sympy as sym
import autograd as ag
import autograd.numpy as np
import autograd.numpy as anp
from sympy import lambdify
from sympy.printing.lambdarepr import NumPyPrinter

array2mat = [{'ImmutableDenseMatrix': np.matrix}, np]


from itertools import chain
from autograd import elementwise_grad, jacobian


def elementwise_hess(fun, argnum=0):
    """
    From https://github.com/HIPS/autograd/issues/60
    """
    def sum_latter_dims(x):
        return anp.sum(x.reshape(x.shape[0], -1), 1)

    def sum_grad_output(*args, **kwargs):
        return sum_latter_dims(elementwise_grad(fun)(*args, **kwargs))
    return jacobian(sum_grad_output, argnum)


def build_functions(sympy_graph, variables):
    logical_np = [{'And': anp.logical_and, 'Or': anp.logical_or}, anp]
    obj = lambdify(tuple(variables), sympy_graph, dummify=True, modules=logical_np)

    def argwrapper(args):
        return obj(*args)

    def grad_func(*args):
        # Note we're mixing anp with np calls here, on purpose
        result = anp.atleast_2d(elementwise_grad(argwrapper)(np.array(np.broadcast_arrays(*args), dtype=np.float)))
        # Put 'gradient' axis at end
        axes = list(range(len(result.shape)))
        result = result.transpose(*chain(axes[1:], [axes[0]]))
        return result

    def hess_func(*args):
        # Note we're mixing anp with np calls here, on purpose
        result = anp.atleast_3d(elementwise_hess(argwrapper)(np.array(np.broadcast_arrays(*args), dtype=np.float)))
        # Put 'hessian' axes at end
        axes = list(range(len(result.shape)))
        result = result.transpose(*chain(axes[2:], axes[0:2]))
        return result

    return obj, grad_func, hess_func


def simple_case_cos():
	def fun_cos(x):
		return np.cos(x)

	dfun_cos = ag.grad(fun_cos)

	domain = np.arange(0, 2*np.pi, .01)

	val = [dfun_cos(x) for x in domain]
	check = -np.sin(domain)
	assert(np.all(np.isclose(val, check)))

def vec_sympy():
	x = sym.symarray('x', 3)

	fun = x[0] + x[1] + x[2]
	fun = sym.lambdify(x, fun, modules=np)

	#dfun = ag.grad(fun)
	def tmp(x):
		return fun(*x)
	dfun = ag.grad(tmp)

	np.random.seed(123)

	domain = np.random.random([100,3])
	analytic_dfun = lambda x: np.array([1., 1., 1.])

	val = [dfun(x) for x in domain]
	check = [analytic_dfun(x) for x in domain]

	assert np.all(np.isclose(val, check))

def inv_matrix_mult():
	def mat1(x):
		#return np.matrix(np.diag(x)) #can't autograd
		return np.diag(x)
	def mat2(x):
		y=2*x
		return np.diag(y)
	def fun(x):
		tmp1 = mat1(x)
		tmp2 = mat2(x)
		print(type(tmp1))
		return np.linalg.inv(np.dot(tmp1, tmp2))

	dfun = ag.jacobian(fun)
	dfun(np.array([1.,2., 3.,4.]))

def sympy_matrix_inv():
	x = sym.symarray('x', 2)

	sym_fun = sym.Matrix([[x[0], x[1]], [x[1], x[0]]])
	#sym_fun = sym.Matrix([[x[0]]])
	#sym_fun = x[0]
	#sym_fun = np.array([[x[0], x[1]], [x[1], x[0]]])
	fun = sym.lambdify(x, sym_fun, modules = array2mat, dummify=False)

	fun2 = lambda x: fun(*x)

	#dfun = ag.jacobian(fun2)
	#dfun(np.array([1.0, 0.0 ]))

	obj, grad, hess = build_functions(sym_fun, x)
	grad(1.0, 0.0 )

def simple_matrix_inv():
	def fun(x):
		return np.linalg.inv(np.diag(x))

	from autograd.numpy import linalg as la
	def fun2(x):
		return la.inv(np.diag(x))

	dfun =ag.jacobian(fun)
	dfun2 =ag.jacobian(fun2)

	print(dfun( np.array([1., 2., 3.])))
	print(dfun2( np.array([1., 2., 3.])))

def simple_sympy_cos():
	x = sym.symbols('x')

	fun = sym.cos(x)
	fun = sym.lambdify(x,fun, modules = np)

	dfun = ag.grad(fun)

	domain = np.arange(0, 2*np.pi, .01)

	val = [dfun(x) for x in domain]
	check = -np.sin(domain)
	assert(np.all(np.isclose(val, check)))

def simple_multi_scalar_case():
	def fun_add(x):
		return x[0]+x[1]

	dfun = ag.grad(fun_add)

	np.random.seed(123)

	domain = np.random.random([100,2])

	val = [dfun(x) for x in domain]
	check = np.ones_like(domain)

	assert(np.all(np.isclose(val, check)))

def simple_vec_return():
	def fun_identity(x):
		return np.array([x[0], x[1], x[2]])

	dfun = ag.jacobian(fun_identity)

	domain = np.random.random([100,3])

	val = [dfun(x) for x in domain]
	check = [np.eye(3) for _ in domain]

	assert(np.all(np.isclose(val, check)))

def simple_tensor_return():
	'''
	not sure if my interpretation of the returned tensor is just wrong
	'''
	def fun_matrix(x):
		return np.array([[2*x[0], 3*x[1]], [x[0], x[1]] ])

	def analytic_dfun(x):
		return np.array([
		[[ 2., 0.], [1., 0.]],
		[[0., 1.], [0., 1.]]])
	dfun = ag.jacobian(fun_matrix)

	domain = np.random.random([100,2])

	val = [dfun(x) for x in domain]
	check = [analytic_dfun(x) for x in domain]

	assert(np.all(np.isclose(val, check)))

def matrix_multiply():
	def fun(x):
		return np.array([[ 2*x[0], 3*x[1]], [x[0], x[1]] ])@x

	dfun = ag.jacobian(fun)
	def analytic_dfun(x):
		return np.array([[ 4*x[0], 6*x[1] ], [2*x[0], 2*x[1]] ])

	domain = np.random.random([100,2])

	val = [dfun(x) for x in domain]
	check = [analytic_dfun(x) for x in domain]

	assert(np.all(np.isclose(val, check)))


if __name__ == '__main__':
	simple_case_cos()
	simple_sympy_cos()
	simple_multi_scalar_case()
	simple_vec_return()
	#simple_tensor_return()
	matrix_multiply()
	vec_sympy()
	simple_matrix_inv()
	inv_matrix_mult()
	sympy_matrix_inv()

